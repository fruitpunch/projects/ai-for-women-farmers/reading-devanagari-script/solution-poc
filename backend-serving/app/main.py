from fastapi import FastAPI, UploadFile
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
from aiofiles import open
from pathlib import Path

Path("./blob/raw").mkdir(parents=True, exist_ok=True)

class UploadResult(BaseModel):
    filename: str

class HealthResult(BaseModel):
    filename: str

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/health") # TODO: type
async def health():
    return {"message": "Hello World"}

@app.post("/read")
async def read_agreement(file: UploadFile):
    async with open(f"./blob/raw/{file.filename}", 'wb') as out_file:
        content = await file.read()
        await out_file.write(content)
    return UploadResult(filename=file.filename)