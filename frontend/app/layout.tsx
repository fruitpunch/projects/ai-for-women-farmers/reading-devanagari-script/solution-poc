import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { Toaster } from "sonner";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Reading Devanagari Frontend",
  description: "Allows users to upload a photo / scan of a loan agreement and get a processed and translated version of it.",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <Toaster className="dark:hidden" />
      <Toaster theme="dark" className="hidden dark:block" />
      <body className={inter.className}>{children}</body>
    </html>
  );
}
