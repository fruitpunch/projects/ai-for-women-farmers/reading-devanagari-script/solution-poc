import Uploader from "@/components/uploader";
import { env } from '@/src/env/env';
import { unstable_noStore } from "next/cache";
 
type EnvVars = {
  BACKEND_HOST: string
}

async function getEnvVars() {
  unstable_noStore();
  return {
    BACKEND_HOST: env.BACKEND_HOST
  }
}
 
export default async function Home() {
  const envVars = await getEnvVars();
  console.log(envVars);
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <Uploader backend_host={envVars.BACKEND_HOST}/>
    </main>
  );
}

